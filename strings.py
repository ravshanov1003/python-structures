"""
Check whether the input string is palindrome.
"""


def main():
    """Check palindrome."""
    s = input()
    rev = ''.join(reversed(s))
    if rev == s:
        print("yes")
    else:
        print("no")


if __name__ == "__main__":
    main()
