"""
split_join.py (add by yourself): Given a string, 
you need to reverse the order of characters in each word within a sentence while still preserving whitespace and
initial word order.
"""


def main():
    """."""

    n = input()
    arr = []
    arr2 = []
    arr = n.split()
    for i in arr:
        i = ''.join(reversed(i))
        arr2.append(i)
    print(' '.join(arr2))


if __name__ == "__main__":
    main()
