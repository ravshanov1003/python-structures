from mock import patch
import pytest

import sets


@pytest.mark.parametrize("lists, expected", [
    (("1", "2"), []),
    (("", ""), []),
    (("2", "1 2"), [2]),
    (("2 1", "2 1"), [1, 2]),
    (("1 1 2 3 5 8 13 21 34 55 89", "1 2 3 4 5 6 7 8 9 10 11 12 13"), [1, 2, 3, 5, 8, 13]),
])
@patch('builtins.input')
def test_main(input_mock, lists, expected):
    input_mock.side_effect = lists
    with patch('builtins.print') as print_mock:
        sets.main()
        print_mock.assert_called_with(expected)
