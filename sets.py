"""
Find common items in 2 lists without duplicates. Sort the result list before output.
"""


def main():
    """Find common numbers."""
    # "1 1 2 3 5 8 13 21 34 55 89", "1 2 3 4 5 6 7 8 9 10 11 12 13"
    li1 = list(map(int, input().split()))
    li2 = list(map(int, input().split()))
    li3 = []

    for i in li1:
        if i in li2 and i not in li3:
            li3.append(i)

    li3.sort()
    print(li3)


if __name__ == "__main__":
    main()
