"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = int(input())
    s = 0
    if n >= 0:
        for i in str(n):
            s = s + int(i)
        print(s)


if __name__ == "__main__":
    main()
