"""
 Drop empty items from a dictionary.
"""
import json

def main():
    """Drop empty items from a dictionary."""
    d = json.loads(input(""))
    my_dict = {}
    for k in d.keys():
        if d[k] is not None:
            my_dict[k] = d[k]

    print(my_dict)


if __name__ == "__main__":
    main()
