"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each command
in order and perform the corresponding operation on your list.
The first line contains an integer, denoting the number of commands.
Each line  of the  subsequent lines contains one of the commands described above.
"""


def main():
    """Perform list commands."""
    n = int(input())
    arr = []

    def math(a, b=0, c=0):
        if a == "insert":
            return arr.insert(int(b), int(c))
        if a == "remove":
            return arr.remove(int(b))
        if a == "append":
            return arr.append(int(b))
        if a == "sort":
            return arr.sort()
        if a == "pop":
            return arr.pop()
        if a == "reverse":
            return arr.reverse()
        if a == "print":
            return print(arr)
    for i in range(n):
        values = input().split()
        a = values[0] if len(values) > 0 else ""
        b = values[1] if len(values) > 1 else ""
        c = values[2] if len(values) > 2 else ""
        math(a, b, c)
        i += 1


if __name__ == "__main__":
    main()
